const { resolveSoa } = require('dns');
const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

const {Pool} = require('pg');
const pool = new Pool({
  //connectionString: 'postgres://postgres:186714569@localhost/Rectangle'
  
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false
  }
  
});

express()
  .use(express.json())
  .use(express.urlencoded({extended:false}))
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .get('/rectangleDB', (req,res) => {
    const getUsersQuery = `Select * from rect`
    pool.query(getUsersQuery, (error,result) => {
      if (error)
        res.end(error);
      const results = {'rows':result.rows}
      res.render('pages/rectangleDB',results);
    })
  })
  .post('/rectEdit', (req,res) => {
    const getUsersQuery = `Select * from rect where name='${req.body.rectName}'`
    pool.query(getUsersQuery, (error,result) => {
      if (error)
        res.end(error);
      const results = {'rData':result.rows}
      res.render('pages/rectEdit',results);
    })
  })
  .post('/applyEdit', (req,res) => {
    const getUsersQuery = `update rect set name='${req.body.newRectName}',
    width=${req.body.newRectWidth},
    height=${req.body.newRectHeight},
    color='${req.body.newRectColor}' where name='${req.body.prevName}'`
    pool.query(getUsersQuery, (error,result) => {
      if (error)
        res.end(error);
      res.render('pages/applyEdit');
    })
  })
  .post('/rectDelete', (req,res) => {
    const getUsersQuery = `delete from rect where name='${req.body.delName}'`
    pool.query(getUsersQuery, (error,result) => {
      if (error)
        res.end(error);
      res.render('pages/rectDelete');
    })
  })
  .get('/rectNew', (req,res) => { res.render('pages/rectNew') })
  .post('/rectAdded', (req,res) => {
    const getUsersQuery = `insert into rect values ('${req.body.newRectName}',
    ${req.body.newRectWidth},
    ${req.body.newRectHeight},
    '${req.body.newRectColor}')`
    pool.query(getUsersQuery, (error,result) => {
      if (error)
        res.end(error);
      res.render('pages/rectAdded');
    })
  })
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
